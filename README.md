# European Data Portal - Gazetteer

A Gazetteer is a geographical dictionary that links place names to coordinates or the other way around to help users to locate and find data sets available in the regions of interest. Primary function of the Gazetteer is to expose the Open Government Data (OGD) that is available for a certain "Place" (e.g. a city, country, district …). It provides the spatial reference (e.g. points with X/Y coordinates) for a place, which is then used to link to the geospatial metadata from the OGD.  This "linking" through actual geospatial references is better than pure "textual" searches as they can be ambiguous or have multiple meanings (e.g. cities that have the same name). Besides supporting search a Gazetteer can also help to improve the quality of metadata that is inserted into the repository.

## Installation

The Gazetteer consists of two parts. The index is based on [con terra smart.finder](http://conterra.de/en/produkte/con-terra-solutionplatform/smartfinder/beschreibung) which is generated by different [FME](http://conterra.de/en/produkte/fme-spatial-etl/fme-die-spatial-etl-technologie/fme-technologie.aspx) processes.
Individual installation instructions are located within the subfolders.


## Obtaining a smart.finder and FME License

In order to use this software component you need a license for
[con terra smart.finder](http://conterra.de/en/produkte/con-terra-solutionplatform/smartfinder/beschreibung) and [FME](http://conterra.de/en/produkte/fme-spatial-etl/fme-die-spatial-etl-technologie/fme-technologie.aspx) .
Evaluation licenses will be available on request. For further information,
please get in touch with:

* [Marc Kleemann (or Thore Fechner)](mailto:edp@conterra.de)
