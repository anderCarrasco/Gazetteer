# European Data Portal - Gazetteer

## INTRODUCTION

A Gazetteer is a geographical dictionary that links place names to coordinates or the other way around to help users to locate and find data sets available in the regions of interest. Primary function of the Gazetteer is to expose the Open Government Data (OGD) that is available for a certain "Place" (e.g. a city, country, district �). It provides the spatial reference (e.g. points with X/Y coordinates) for a place, which is then used to link to the geospatial metadata from the OGD.  This "linking" through actual geospatial references is better than pure "textual" searches as they can be ambiguous or have multiple meanings (e.g. cities that have the same name). Besides supporting search a Gazetteer can also help to improve the quality of metadata that is inserted into the repository.

## Purpose and Scope

The objectives of this task are to integrate in the Open Data Portal a gazetteer functionality that will help users to find open data for certain places using geographical names spatial datasets provided by INSPIRE and other sources. This is according to data specification in version 3.198:

_"__Search referring to place names must be supported by the implementation of a gazetteer that shall make use of the INSPIRE annex I geographical names spatial datasets in the Member States12. Integration with other place identifiers from extensively used linked data resources will be positively evaluated. The gazetteer functionality must support local caching as well as real time remote access to Member States geographical names spatial datasets."_

This search is user friendly, intuitive, with good performance and integrated into the web environment using the RESTful endpoint of the gazetteer. The consortium implemented the gazetteer with the FME (Feature Manipulation Engine) to support remote access to Member states geographical names spatial datasets and to fill a local cache � an Apache SOLR Lucene Index � on basis of con terra's smart.finder technology. The gazetteer provides information for data search for locations in different languages. For example a user in the Netherlands will be able to search for data in the German capital Berlin by typing its Dutch name "Berlijn", a French speaking person will be able to search data for London by typing in "Londres".


## Installation Guide
### Gazetteer Harvesting � FME Server

- Install FME Server
    - Relevant FME Server documentation:
        -   [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#AdminGuide/Before\_You\_Begin.htm](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#AdminGuide/Before_You_Begin.htm)
        -   [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#AdminGuide/Configuring\_FME\_Server\_to\_Send\_Email.htm](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#AdminGuide/Configuring_FME_Server_to_Send_Email.htm)
        -  [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#WebUI/schedules.htm?Highlight=schedule](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#WebUI/schedules.htm?Highlight=schedule)
        -  [http://docs.safe.com/fme/html/FME\_Server\_Documentation/Default.htm#WebUI/Cleanup.htm](http://docs.safe.com/fme/html/FME_Server_Documentation/Default.htm#WebUI/Cleanup.htm)
- Install PostgreSQL with PostGIS extension (default installation)
- Configure FME Server
    - Create a Topic called "odp-failure" and configure a Subscription to send an Email
    - The email configured will be sent whenever an error occurs during the harvesting processes
- Publish all FME Workspaces via FME Workbench
- Create a Scheduled Process
    - Select "serverJobSubmitter.fmw" as a main Workspace
    - Fill in your database credentials
    - Fill in your FME Server credentials
    - Fill in the solr URL used to swap your cores
    - Fill in the solr URL to delete your background core
    - Fill in the solr credentials
- Configure Cleanup Tasks
- For the merging process as PostgreSQL/ PostGIS database has to be existing
    - SQL Statement:
    "CREATE DATABASE gazetteer; CREATE EXTENSION postgis;"

#### Logging

All FME processes create a log file which can be accessed via the web interface of the FME Server. The log files will be removed after a predefined time which can be configured within the FME Cleanup Tasks (see previous chapter).

### Troubleshooting

What can I do if the REST interface is not available?

- Make sure that the smart.finder is deployed correctly and that the service is running

A FME constantly fails:

- Please read the log file of the process to get detailed information about the error.
- Common problems could be:
  - o.The PostGIS database is not accessible
  - o.The source to be harvested is not available or invalid
  - o.The smart.finder endpoint can't be accessed

Gazetteer entries of a country are missing:

- Check if the corresponding FME job has succeeded
- If the job was successful but didn't produce any outputs, check the original data source

## Infrastructure specifications
#### Server

For hosting smart.finder the following server requirements need to be fulfilled:

#### Operating Systems

- Windows Server 2008 R2 32bit/64bit
- Windows Server 2003 R2 32bit/64bit
- Windows XP Professional SP 3
- Linux 64-bit Kernel 2.6
- Linux 32-bit Kernel 2.6

#### Runtime Environment

smart.finder components are provided as Java web applications that run in a servlet container. Recommended products and versions:

- Tomcat 7.0.x [7.0.19] mit JDK 1.7.0 u25 (oder h�her) [1.7.0\_25-b15 / 1.7.0\_25-b17] 32bit oder JDK 1.7.0 u25 (oder h�her) [1.7.0\_25-b15 / 1.7.0\_25-b17] 64bit
- Tomcat 8.0.x [8.0.14] mit JDK 1.8.x [1.8.0\_25-b18] 32bit oder JDK 1.8 [1.8.0\_25-b18] 64bit

#### Web-Server (optional)

Any HTTP server (e.g. Apache) can be used as web server. The only requirement is that Apache Tomcat is integrated as the servlet/JSP engine. Apache Tomcat can also be used as a web server directly.

#### Esri ArcGIS API for JavaScript

smart.finder builds on the Esri ArcGIS API for JavaScript and therefore requires a valid and sufficient license of the Esri ArcGIS API for JavaScript to operate. Please contact your local distributor for details. Technically, the API can be referenced externally or included locally.

### FME Server

Technical Requirements for the FME Server can be found on the official Safe Software website:
  [http://www.safe.com/fme/fme-server/tech-specs/](http://www.safe.com/fme/fme-server/tech-specs/)
  
## Obtaining a smart.finder and FME License

In order to use this software component you need a license for
[con terra smart.finder](http://conterra.de/en/produkte/con-terra-solutionplatform/smartfinder/beschreibung) and [FME](http://conterra.de/en/produkte/fme-spatial-etl/fme-die-spatial-etl-technologie/fme-technologie.aspx) .
Evaluation licenses will be available on request. For further information,
please get in touch with:

* [Marc Kleemann (or Thore Fechner)](mailto:edp@conterra.de)
